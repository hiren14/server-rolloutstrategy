const jwt = require("jsonwebtoken");
const privateKey = process.env.ACCESS_TOKEN_SECRET;

module.exports = function(req, res, next) {
  const token = req.headers["token"];
  
  if (!token) return res.status(401).json({ message: "Auth Error" });

  try {
    const decoded = jwt.verify(token, privateKey);
    req.user = decoded.user;
    
    next();
  } catch (error){
    if (error.name === "TokenExpiredError") {
      return res
        .status(401)
        .json({ error: "Session timed out,please login again" });
    } else if (error.name === "JsonWebTokenError") {
      return res
        .status(401)
        .json({ error: "Invalid token,please login again!" });
    } else {
      //catch other unprecedented errors
      console.error(error);
      return res.status(400).json({ error });
    }
  }
};
