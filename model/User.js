const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const Token = require("./Token");

const { ACCESS_TOKEN_SECRET, REFRESH_TOKEN_SECRET } = process.env;

const userSchema = new mongoose.Schema({
  orgname:{
    type:String,
    required:true
  },
  username: {
    type: String,
    required: true,
    unique:true
  },
  password: {
    type: String,
    required: true
  },
  usertype:{
    type:String,
    required:true
  }

});

userSchema.methods = {
  createAccessToken: async function () {
    try {
      let { _id,username,usertype } = this;
      let accessToken = jwt.sign(
        { user: { _id,username,usertype } },
        ACCESS_TOKEN_SECRET,
        {
          expiresIn: 3600,
        }
      );
      return accessToken;
    } catch (error) {
      console.error(error);
      return;
    }
  },
  createRefreshToken: async function () {
    try {
      let { _id,username,usertype } = this;
      let refreshToken = jwt.sign(
        { user: { _id,username,usertype } },
        REFRESH_TOKEN_SECRET,
        {
          expiresIn: "1d",
        }
      );

      await new Token({ token: refreshToken }).save();
      return refreshToken;
    } catch (error) {
      console.error(error);
      return;
    }
  },
};

//pre save hook to hash password before saving user into the database:


// export model user with UserSchema
module.exports = mongoose.model("userDetails", userSchema);
