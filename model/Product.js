const mongoose = require("mongoose");

const ProductSchema = mongoose.Schema({
  orgname: {
        type: String,
        required: true
      },
  productName: {
    type: String,
    required: true
  },
  productCode:{
    type:String,
    required:true
  },
  businessUnit:{
      type:String,
      required:true
  }


});


module.exports = mongoose.model("products", ProductSchema);