const mongoose = require("mongoose");

const BUSchema = new mongoose.Schema({
    orgname: { type: String,required:true },
    businessUnit: { type: String,required:true },
    
  });
  
module.exports = mongoose.model("bu", BUSchema);