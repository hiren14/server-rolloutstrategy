const mongoose = require("mongoose");

const TemplateSchema = mongoose.Schema({
  templateName: {
    type: String,
    required: true
  },
  questions:{
    type:Array,
    required:true
  }

});


module.exports = mongoose.model("templates", TemplateSchema);