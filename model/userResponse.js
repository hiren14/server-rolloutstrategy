const mongoose = require("mongoose");

const UserResponseSchema = mongoose.Schema({
 
  username:{
      type:String,
      required:true
  },
  usertype:{
    type:String,
    required:true
  },
  productName:{
    type:String,
    required:true
  },
  productCode:{
    type:String,
    required:true
  },
  response: {
    type:Array,
    required: true
  },
  
  tid:{
    type:String,
    required:true
  }
});

// export model user with UserSchema
module.exports = mongoose.model("userResponse", UserResponseSchema);