const mongoose = require("mongoose");

const programOwnerTemplateSchema = mongoose.Schema({
  orgname:{
    type:String,
    required:true
  },
  templateName: {
    type: String,
    required: true
  },
  questions:{
    type:Array,
    required:true
  },
  tid:{
      type:String,
      required:true
  },
  visibility:{
    type:Array,
    required:true
  }

});


module.exports = mongoose.model("programownertemplate", programOwnerTemplateSchema);