const mongoose = require("mongoose");

const QuestionSchema = mongoose.Schema({
  question: {
    type: String,
    required: true
  },
  options:{
    type:Array,
    required:true
  },
  visibility:{
    type:Array,
    required:true
  }

});

// export model user with UserSchema
module.exports = mongoose.model("questions", QuestionSchema);