require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const user = require("./routes/user");
const admin = require("./routes/admin");
const programOwner = require("./routes/programOwner");
const cors=require('cors');
const InitiateMongoServer = require("./config/db");

// Initiate Mongo Server
InitiateMongoServer();

var corsOptions = {
  origin: "http://localhost:3000"
};


const app = express();
app.use(cors(corsOptions));
// PORT
const PORT = process.env.PORT || 4000;

// Middleware
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.json({ message: "API Working" });
});

/**
 * Router Middleware
 * Router - /user/*
 * Method - *
 */
app.use("/user", user);
app.use("/admin", admin);
app.use("/programOwner",programOwner)

app.listen(PORT, (req, res) => {
  console.log(`Server Started at PORT ${PORT}`);
});
