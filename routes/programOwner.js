const express = require("express");

const router = express.Router();
const auth = require("../middleware/auth");

const bcrypt = require("bcryptjs");
const Template = require("../model/Template");
const programOwnerTemplate = require("../model/programOwnerTemplate");
const userResponse = require("../model/userResponse");
const User = require("../model/User");
const Product =require("../model/Product")
const BU=require("../model/BU")




/*
this route will list all the templates created by the program owner of a
particular organization and will send it to the client side
*/
router.get("/home", auth, async (req, res) => {
  try {
    
    const ut=req.user.usertype
    if(ut!="programowner")
      return res.status(400).send("Invalid usertype")
    const po= await User.findOne({username:req.user.username})
    const templates= await programOwnerTemplate.find({orgname:po.orgname})
    res.status(200).json(templates);
  } catch (e) {
    res.status(500).send("Internal server error");
  }
});



/*
this route will add the business unit details into the bu collection
and returns the confirmation for the same
*/
router.post(
  "/addBU",
  auth,
  async (req, res) => {
    

    const {orgname, businessUnit } = req.body;
    
    try {
      let bu = await BU.findOne({
        orgname,
        businessUnit
      });
      if (bu) {
        return res.status(400).send(
        "Business Unit Already Exists"
        );
      }

      bu = new BU({
        orgname,
        businessUnit
      });
      
      await bu.save();

      res.status(200).json({
            message:"Business Unit added successfully"
          });
       
    } catch (err) {
      if(err.name==="ValidationError")
        res.status(400).send("Please enter valid details")
      else
        res.status(500).send("Internal server error");
    }
  }
);


/*
this route will list all the business units created by the admin and programowner
for a particular organization and will send it to the client side
*/
router.get('/getBU',auth,async(req,res)=>{

  try {
    
    const ut=req.user.usertype
    if(ut!="programowner")
       return res.status(400).send("Invalid usertype")
    const po= await User.findOne({username:req.user.username})
    
    const bus= await BU.find({orgname:po.orgname})
    res.status(200).json({"bus":bus,"orgname":po.orgname});
  } catch (e) {
    res.status(500).send("Internal server error");
  }

})


/*
this route will edit the particular business unit details
*/
router.put('/editBU',auth,async(req,res)=>{

  try {
    
    await BU.findByIdAndUpdate({_id:req.body.buid},{businessUnit:req.body.businessUnit})
    res.status(200).json({
      message:"Business Unit edited successfully"
    });
  } catch (e) {
    res.status(500).send("Internal server error");
  }

})


/*
this route will delete the particular business unit
*/
router.delete('/deleteBU',auth,async(req,res)=>{

  try {
    
    await BU.findByIdAndRemove({_id:req.body.buid})
    res.status(200).json({
      message:"Business Unit deleted successfully"
    });
  } catch (e) {
    res.status(500).send("Internal server error");
  }

})






/*
this route will list all the products created by the program owner of 
a particular organization and will send it to the client side
*/
router.get("/getProducts", auth, async (req, res) => {
  try {
    
    const ut=req.user.usertype
    if(ut!="programowner")
      return res.status(400).send("Invalid usertype")
    const po= await User.findOne({username:req.user.username})
    const products= await Product.find({orgname:po.orgname})
    if(req.query.count==0)
    {
      
    
      const bus= await BU.find({orgname:po.orgname})
    res.status(200).json({"products":products,"bus":bus});
    }
    else
    {
      res.status(200).json({"products":products});
    }
  } catch (e) {
    res.status(500).send("Internal server error");
  }
});


/*
this route will add the product details into the product collection
and returns the confirmation for the same
*/
router.post("/addProduct",auth,async (req,res)=>{

  try
  {
    const po= await User.findOne({username:req.user.username})
    const orgname=po.orgname 
    const {productName,productCode,businessUnit}=req.body
    const d=new Product({
      orgname,
      productName,
      productCode,
      businessUnit
    })
    await d.save()
    res.status(200).json({
      message:"Product Saved successfully"
    });
  }
  catch(e)
  {
    res.status(500).send("Internal server error");
  }

})



/*
this route will add edit the particular product
*/
router.put("/editProduct",auth,async (req,res)=>{

  try
  {
    
    const {pid,productName,productCode,businessUnit}=req.body
    
    await Product.findByIdAndUpdate({_id:pid},{"productName":productName,"productCode":productCode,"businessUnit":businessUnit})
    res.status(200).json({
      message:"Product Edited successfully"
    });
  }
  catch(e)
  {
    
    res.status(500).send("Internal server error");
  }

})




/*
this route will delete the particular product
*/
router.delete("/deleteProduct", auth, async (req, res) => {
    
  
  try{
  
    await Product.findByIdAndRemove({_id:req.body.pid})
    
    res.status(200).json({
        message:"Product deleted successfully"
      });
  }
  
  
 catch (e) {
  res.status(500).send("Internal server error");
}
});



/*
this route will list all the users created by the program owner and
the admin for the particular organization and will send it to client side
*/
router.get("/getUsers", auth, async (req, res) => {
  try {
    
    const ut=req.user.usertype
    if(ut!="programowner")
      return res.status(400).send("Invalid usertype")
    const po= await User.findOne({username:req.user.username})
    const temp= await User.find({orgname:po.orgname})
    const users=temp.filter((item)=>item.username!==po.username)
    if(req.query.count==0)
    {
    const bus=await BU.find({orgname:po.orgname})
    res.status(200).json({"users":users,"orgname":po.orgname,"bus":bus});
    }
    else
    {
      res.status(200).json({"users":users,"orgname":po.orgname});
    }
    
  } catch (e) {
    res.status(500).send("Internal server error");
  }
});




/*
this route will add the user details into the user collection
and returns the confirmation for the same
*/
router.post(
  "/addUser",
  auth,
  async (req, res) => {
    

    const { orgname,username, password,usertype } = req.body;
    
    try {
      let user = await User.findOne({
        username
      });
      if (user) {
        return res.status(400).send(
        "User Already Exists"
        );
      }

      
      user = new User({
        orgname,
        username,
        password,
        usertype
      });
      

      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);

      await user.save();

      

      
      res.status(200).json({
            message:"User added successfully"
          });
       
    } catch (err) {
      
      if(err.name==="ValidationError")
        res.status(400).send("Please enter valid details")
      else
        res.status(500).send("Internal server error");
    }
  }
);



/*
this route will edit the particular user
*/
router.put(
  "/editUser",
  auth,
  async (req, res) => {
    

    const { uid,username, password,usertype } = req.body;
    
    try {
  
      if(password && password!=="")
      {

      const salt = await bcrypt.genSalt(10);
      const password1 = await bcrypt.hash(password, salt);

      await User.findByIdAndUpdate({_id:uid},{username:username,password:password1,usertype:usertype});
      }
      else
      {
        
        await User.findByIdAndUpdate({_id:uid},{username:username,usertype:usertype});
      }
      

      
      res.status(200).json({
            message:"User edited successfully"
          });
       
    } catch (err) {
      
      if(err.name==="ValidationError")
        res.status(400).send("Please enter valid details")
      else
        res.status(500).send("Internal server error");
    }
  }
);




/*
this route will delete the particular user
*/
router.delete("/deleteUser", auth, async (req, res) => {
    
  
  try{
  
    await User.deleteOne({username:req.body.username})
    
    await userResponse.deleteOne({username:req.body.username})

    res.status(200).json({
        message:"User deleted successfully"
      });
  }
  
  
 catch (e) {
  res.status(500).send("Internal server error");
}
});




/*
this route will either add the template or update the template 
into the program owner template collection
*/
router.post("/saveTemplate", auth, async (req, res) => {
    
      
      const {templateName,questions,visibility,tid,flag}=req.body
      const po= await User.findOne({username:req.user.username})
      const orgname=po.orgname
      const d=await programOwnerTemplate.findOne({"tid":tid})
      if(flag)
      {
        try{

     
            await programOwnerTemplate.findByIdAndUpdate({_id:req.body.id},{"templateName":templateName,"questions":questions,"visibility":visibility})
      
            res.status(200).json({
                message:"Template Edited successfully"
              });
          }
          catch (e) {
            res.status(500).send("Internal server error");
          }   
      }
      else
      {
      try{

        templateData=new programOwnerTemplate({
            orgname,
            templateName,
            questions,
            visibility,
            tid
        })
        await templateData.save()

        res.status(200).json({
            message:"Template added successfully"
          });
      }
    
      
     catch (e) {
      res.status(500).send("Internal server error");
    }
}
  });



/*
this route will delete the particular template from 
program owner template collection
*/
router.delete("/deleteTemplate", auth, async (req, res) => {
    
   
  try{
  
    await programOwnerTemplate.findByIdAndRemove({_id:req.body.tid})
    await userResponse.deleteMany({tid:req.body.tid})
    res.status(200).json({
        message:"Template deleted successfully"
      });
  }
  
  
 catch (e) {
  res.status(500).send("Internal server error");
}
});




/*
this route will list all the templates created by the admin 
and will send it to client side
*/
router.get("/getAdminTemplates", auth, async (req, res) => {
    
    try {
      
      const ut=req.user.usertype
      if(ut!="programowner")
        return res.status(400).send("Invalid usertype")
      
      const templates= await Template.find()
      res.status(200).json(templates);
    } catch (e) {
      res.status(500).send("Internal server error");
    }
  });



/*
this route will list the particular template requested by program owner
either from program owner template collection or template collection
*/
router.get("/getTemplate", auth, async (req, res) => {
    
    try {
      
      const ut=req.user.usertype
      if(ut!="programowner")
        return res.status(400).send("Invalid usertype")
      const po= await User.findOne({username:req.user.username})
      const bus=await BU.find({orgname:po.orgname})
      var template=await programOwnerTemplate.findOne({_id:req.query.tid})
      
      if(template)
      {
        
        res.status(200).json({"flag":true,template,bus});
      } 
      else
      {
       template= await Template.findById({_id:req.query.tid})
      
      res.status(200).json({"flag":false,template,bus});
      }
      
      
      
      
      
    } catch (e) {
      res.status(500).send("Internal server error");
    }
});



/*
this route will list all the responses of business users for a partcular template, 
perform some calculations and will send it to the client side
*/
router.get("/getResponses", auth, async (req, res) => {
    
    try {
      
      const ut=req.user.usertype
      
      if(ut!="programowner")
        return res.status(400).send("Invalid usertype")
      const responses=await userResponse.find({tid:req.query.tid})
      
      
      if(responses.length>0){
        var fr=[]
       
        for(let i=0;i<responses.length;i++)
        {
          
          const bu=responses[i].usertype
          const pn=responses[i].productName
          const pc=responses[i].productCode
          
          let r=responses[i].response
          var bv=0
          var ic=0
          for(let j=0;j<r.length;j++)
          {
            if(r[j].category=="ic")
            {
              ic+=r[j].ans[1]
            }
            else
            {
              bv+=r[j].ans[1]
            }
          }
          const ps=bv+ic
          const rank=1
          
          fr.push({bu,pn,pc,bv,ic,ps,rank})
        }

        
        var index=0
        var index2=1
        var l=fr.length
        
        while(index<l) {
          
          let count=1
          while(index2<l) {
      
              if (fr[index2].bu == fr[index].bu && fr[index2].pn == fr[index].pn) {
                  
                  

                  fr[index].ic = fr[index].ic*count+fr[index2].ic;
                  fr[index].bv = fr[index].bv*count+fr[index2].bv;
                  count+=1
                  fr[index].bv = Number((fr[index].bv/count).toFixed(2));
                  fr[index].ic = Number((fr[index].ic/count).toFixed(2));
                  fr[index].ps = fr[index].bv+fr[index].ic;
                  

                  
                  
                  fr.splice(index2,1)
                  l-=1
                  index2-=1
                  
                  
              }
              
                index2+=1
              
          }
          index+=1
          index2=index+1
      }
       
          
     
      fr.sort(function(a, b) {
          return b.ps - a.ps;
      });

       var rank=1
       for(let i=0;i<fr.length;i++)
       {
          if(i==0)
          {
            fr[i].rank=rank
          }
          else if(i>0 && fr[i-1].ps==fr[i].ps)
          {
            fr[i].rank=rank
          }
          else if(i>0 && fr[i-1].ps!=fr[i].ps)
          {
            rank+=1
            fr[i].rank=rank
          }

       }
        res.status(200).json({"status":"success",fr});
      }
      else
      {
        res.status(200).json({"status":"no response"})
      }
      
    } catch (e) {
      
      res.status(500).send("Internal server error");
    }
  });





/*
this route will list all the responses of business users for a partcular template, 
get products name from responses and assign the same to the users.
*/
router.get("/getSurveyCompletionReport", auth, async (req, res) => {
    try {
      
      const ut=req.user.usertype
      if(ut!="programowner")
        return res.status(400).send("Invalid usertype")
      const po= await User.findOne({username:req.user.username})
      const users=await User.find({orgname:po.orgname})
      const responses=await userResponse.find({tid:req.query.tid})
      var scr=[]
      for(let i=0;i<users.length;i++)
      {
        let pcs=""
        for(let j=0;j<responses.length;j++)
        {
          if(responses[j].username==users[i].username)
          {
            pcs+=responses[j].productName
            pcs+=" , "
          }
        }
        pcs=pcs.slice(0,-2)
        if(users[i].usertype!="programowner")
          scr.push({user:users[i].username,pcs:pcs})

      }
      res.status(200).json(scr);
    } catch (e) {
      res.status(500).send("Internal server error");
    }
  });



module.exports = router;
