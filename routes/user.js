const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const router = express.Router();
const auth = require("../middleware/auth");
const { ACCESS_TOKEN_SECRET, REFRESH_TOKEN_SECRET } = process.env;
const User = require("../model/User");
const programOwnerTemplate = require("../model/programOwnerTemplate");
const Token = require("../model/Token");
const Otp = require("../model/OTP");
const userResponse = require("../model/userResponse");
const sendMail=require('../middleware/sendMail')
const Product =require("../model/Product")



/*
this route will validate the user credentials and return the
confirmation for the same
*/
router.post(
  "/login",
  
  async (req, res) => {
    

    const { username, password } = req.body;
    try {
      let user = await User.findOne({
        username
      });
      if (!user)
      return res.status(400).send(
        "User does not exists"
        );

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch)
      return res.status(400).send(
        "Incorrect Password"
        );

      
      
      let accessToken = await user.createAccessToken();
      let refreshToken = await user.createRefreshToken();

      

      return res.status(201).json({ user:{username:user.username,usertype:user.usertype}, accessToken, refreshToken });
    } 
    catch (e) {
      res.status(500).send("Internal server error");
    }
  }
);


router.post('/forgetPassword',async (req,res)=>{

  const { email } = req.body
  
  try
  {
    let user = await User.findOne({
      email
    });

    
    if (!user)
    return res.status(400).send(
      "User does not exists"
      );
    
    const otp=Math.floor(100000 + Math.random() * 900000)
    const data={
      user,
      otp,
      html:'<h3>Your one time password is</h3>'+otp.toString()
    }
    
    sendMail.mymailer(data,async function(err,info){
      if(!err)
      {
        const email=user.email
        
        const otpdata=new Otp({
            email,
            otp,
        })
        
        otpdata.otpExpires=Date.now()+3600000*24
        const d=await Otp.findOne({"email":email})
        if(d)
          await Otp.updateOne({"email":email},{otp:otpdata.otp,otpExpires:otpdata.otpExpires})
        else
          await otpdata.save()
        res.status(200).json({"message":"Otp has been sent to your mail",id:user._id})
      }

      else
      {
        res.status(500).send("Internal server error");
      }
    })

  }
  catch(e)
  {
    res.status(500).send("Internal server error");
  }

})

router.post('/verifyOtp',async (req,res)=>{

  const {otp,email}=req.body
  

  try{
  
  const otpdata=await Otp.findOne({"email":email,"otp":otp})
  const otpdata1=await Otp.findOne({"email":email,"otp":otp,"otpExpires":{ $gt: Date.now()}})
  if(otpdata && otpdata1)
  {
    await Otp.deleteOne({"email":email,"otp":otp})
    res.status(200).json({"message":"successfull"})
  }
  else if(otpdata){
    res.status(200).json({"message":"Otp expired"})
  }
  else
  {
    res.status(200).json({"message":"Please enter correct otp"})
  }
}
  catch(e)
  {
    res.status(500).send("Internal server error");
  }

})

router.put('/resetPassword',async (req,res)=>{

  try
  {
    const {id,password}=req.body
    
    
    const salt = await bcrypt.genSalt(10);
    password1 = await bcrypt.hash(password, salt);
    
    await User.findByIdAndUpdate({_id:id},{"password":password1})

    res.status(200).json({"message":"Password resetted successfully."})
  }
  catch(e)
  {

    res.status(500).send("Internal server error");
  }

})




/*
this route will refresh the access token for a particular time
*/
router.post('/refreshToken',async (req,res)=>{

  try {
    //get refreshToken
    const { refreshToken } = req.body;
    //send error if no refreshToken is sent
    if (!refreshToken) {
      return res.status(403).json({ error: "Access denied,token missing!" });
    } else {
      //query for the token to check if it is valid:
      const tokenDoc = await Token.findOne({ token: refreshToken });
      //send error if no token found:
      if (!tokenDoc) {
        return res.status(401).json({ error: "Token expired!" });
      } else {
        //extract payload from refresh token and generate a new access token and send it
        const payload = jwt.verify(tokenDoc.token, REFRESH_TOKEN_SECRET);
        const accessToken = jwt.sign({ user: payload }, ACCESS_TOKEN_SECRET, {
          expiresIn: 3600,
        });
        return res.status(200).json({ accessToken });
      }
    }
  } catch (error) {
    res.status(500).send("Internal server error");
  }
   


})



/*
this route will delete the token from token collection which helps user to 
logout from the application
*/
router.delete('/logout',async (req,res)=>{
  
  try {
  
    const { refreshToken } = req.body;
    await Token.findOneAndDelete({ token: refreshToken });
    return res.status(200).json({ success: "User logged out!" });
  } catch (error) {
    res.status(500).send("Internal server error");
  }

})



/*
this route will list the product and template assigned to a particual user
and will send it to the client side
*/
router.get("/home", auth, async (req, res) => {
  try {
    
    const ut=req.user.usertype
    if(ut=="admin" || ut=="programowner")
      return res.status(400).send("Invalid usertype")
    
    const temp=await Product.find({businessUnit:req.user.usertype}) 
    const temp1=await userResponse.find({username:req.user.username})
    const productData=temp.filter(({productName:id1})=>!temp1.some(({productName:id2})=>id2===id1))
  
    if(productData.length>0)
    {
    const template= await programOwnerTemplate.findOne({visibility:{$in:[req.user.usertype]}})
    if(template)
      res.status(200).json({"message":"ok",template,productData});
    else
    res.status(200).json({"message":"no survey avaialble"});
    }
    else{
      res.status(200).json({"message":"no product avaialble"});
    }
  } catch (e) {
    res.status(500).send("Internal server error");
  }
});




/*
this route will add the response from user into the user response 
collection
*/
router.post("/addResponse", auth, async (req, res) => {
    
  const {response,tid,product}=req.body
  try{
    
    const productData=await Product.findOne({'productName':product})
    const productName=productData.productName
    const productCode=productData.productCode
    
    userResponseData=new userResponse({
        username:req.user.username,
        usertype:req.user.usertype,
        productName,
        productCode,
        response,
        tid
    })
    await userResponseData.save()

    res.status(200).json({
        message:"Response added successfully"
      });
  }
  
  
 catch (e) {
   
  res.status(500).send("Internal server error");
}
});



module.exports = router;
