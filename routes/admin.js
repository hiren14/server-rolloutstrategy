const express = require("express");
const bcrypt = require("bcryptjs");
const router = express.Router();
const auth = require("../middleware/auth");
const userResponse = require("../model/userResponse");
const User = require("../model/User");
const Template = require("../model/Template");
const programOwnerTemplate = require("../model/programOwnerTemplate");
const BU=require("../model/BU")


/*
this route will add the user details into the user collection
and returns the confirmation for the same
*/
router.post(
  "/addUser",
  auth,
  async (req, res) => {
    

    const { orgname,username, password,usertype } = req.body;
    
    try {
      let user = await User.findOne({
        username
      });
      if (user) {
        return res.status(400).send(
        "User Already Exists"
        );
      }

      
      user = new User({
        orgname,
        username,
        password,
        usertype
      });
      

      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);

      await user.save();

      res.status(200).json({
            message:"User added successfully"
          });
       
    } catch (err) {
      if(err.name==="ValidationError")
        res.status(400).send("Please enter valid details")
      else
        res.status(500).send("Internal server error");
    }
  }
);



/*
this route will list all the users created by the admin
and will return the user details to client side
*/
router.get("/getUsers", auth, async (req, res) => {
  try {
    
    const ut=req.user.usertype
    if(ut!="admin")
      return res.status(400).send("Invalid usertype")
    const po= await User.findOne({username:req.user.username})
    const temp= await User.find()
    const users=temp.filter((item)=>item.username!==po.username)
    if(req.query.count==0)
    {
    var bus=[{"businessUnit":"programowner"}]
    var temp1=await BU.find()
    var bus=bus.concat(temp1)
    res.status(200).json({"users":users,"bus":bus});
    }
    else
    {
      res.status(200).json({"users":users});
    }
    
  } catch (e) {
    res.status(500).send("Internal server error");
  }
});




/*
this route will edit the particular user
*/
router.put(
  "/editUser",
  auth,
  async (req, res) => {
    

    const { uid,orgname,username, password,usertype } = req.body;
    
    try {
      
      if(password && password!=="")
      {

      const salt = await bcrypt.genSalt(10);
      const password1 = await bcrypt.hash(password, salt);

      await User.findByIdAndUpdate({_id:uid},{orgname:orgname,username:username,password:password1,usertype:usertype});
      }
      else
      {
        
        await User.findByIdAndUpdate({_id:uid},{orgname:orgname,username:username,usertype:usertype});
      }
      
      res.status(200).json({
            message:"User edited successfully"
          });
       
    } catch (err) {
      
      if(err.name==="ValidationError")
        res.status(400).send("Please enter valid details")
      else
        res.status(500).send("Internal server error");
    }
  }
);



/*
this route will delete the particular user
*/
router.delete("/deleteUser", auth, async (req, res) => {
    
  
  try{
  
    const d=await User.findOne({username:req.body.username})
    await User.deleteOne({username:req.body.username})
    if(d.usertype!="admin" && d.usertype!="programowner")
    
      await userResponse.deleteOne({username:req.body.username})
    
    
    res.status(200).json({
        message:"User deleted successfully"
      });
  }
  
  
 catch (e) {
  res.status(500).send("Internal server error");
}
});




/*
this route will add the business unit details into the bu collection
and returns the confirmation for the same
*/
router.post(
  "/addBU",
  auth,
  async (req, res) => {
    

    const {orgname, businessUnit } = req.body;
    
    try {
      let bu = await BU.findOne({
        orgname,
        businessUnit
      });
      if (bu) {
        return res.status(400).send(
        "Business Unit Already Exists"
        );
      }

      bu = new BU({
        orgname,
        businessUnit
      });
      
      await bu.save();

      res.status(200).json({
            message:"Business Unit added successfully"
          });
       
    } catch (err) {
      if(err.name==="ValidationError")
        res.status(400).send("Please enter valid details")
      else
        res.status(500).send("Internal server error");
    }
  }
);


/*
this route will list all the business units created by the admin
and will send it to the client side
*/
router.get('/getBU',auth,async(req,res)=>{

  try {
    
    const ut=req.user.usertype
    if(ut!="admin")
       return res.status(400).send("Invalid usertype")
    const bus= await BU.find()
    res.status(200).json(bus);
  } catch (e) {
    res.status(500).send("Internal server error");
  }

})


/*
this route will edit the particular business unit details
*/
router.put('/editBU',auth,async(req,res)=>{

  try {
    
    await BU.findByIdAndUpdate({_id:req.body.buid},{orgname:req.body.orgname,businessUnit:req.body.businessUnit})
    res.status(200).json({
      message:"Business Unit edited successfully"
    });
  } catch (e) {
    res.status(500).send("Internal server error");
  }

})


/*
this route will delete the particular business unit
*/
router.delete('/deleteBU',auth,async(req,res)=>{

  try {
    
    await BU.findByIdAndRemove({_id:req.body.buid})
    res.status(200).json({
      message:"Business Unit deleted successfully"
    });
  } catch (e) {
    res.status(500).send("Internal server error");
  }

})




/*
this route will verify the usertype of the user in order
to authenticate the user
*/
router.get('/verifyUsertype',auth,async (req,res)=>{

  const ut=req.user.usertype
  if(ut=="admin")
    await res.status(200).json({"message":"ok"})
  else
    await res.status(400).send("Invalid usertype")

})





/*
this route will list all the templates created by the admin
and will return it to the client side
*/
router.get("/home", auth, async (req, res) => {
  try {
    
    const ut=req.user.usertype
    if(ut!="admin")
       return res.status(400).send("Invalid usertype")
    const templates= await Template.find()
    res.status(200).json(templates);
  } catch (e) {
    res.status(500).send("Internal server error");
  }
});



/*
this route will add the template details into the template collection
and returns the confirmation for the same
*/
router.post("/addTemplate", auth, async (req, res) => {
    
      try{
        
        const {templateName,questions,visibility}=req.body
        templateData=new Template({
            templateName,
            questions,
            visibility
        })
        await templateData.save()

        res.status(200).json({
            message:"Template added successfully"
          });
      }
      
      
     catch (e) {
      res.status(500).send("Internal server error");
    }
  });



/*
this route will list the particular template requested by the admin
and will return the same
*/
router.get("/getTemplate", auth, async (req, res) => {
    
    try {
      
      const ut=req.user.usertype
      if(ut!="admin")
        return res.status(400).send("Invalid usertype")
      const template= await Template.findById({_id:req.query.tid})
      
      res.status(200).json(template);
    } catch (e) {
      res.status(500).send("Internal server error");
    }
  });



/*
this route will edit the template created by the admin
*/
router.put("/editTemplate", auth, async (req, res) => {
    
    
    const {templateName,questions,visibility}=req.body
    console.log(templateName,req.body.tid)
    try{

      await Template.findByIdAndUpdate({_id:req.body.tid},{"templateName":templateName,"questions":questions,"visibility":visibility})

      res.status(200).json({
          message:"Template Edited successfully"
        });
    }
    
    
   catch (e) {
    res.status(500).send("Internal server error");
  }
});


/*
this route will delete the particular template created by the admin
*/
router.delete("/deleteTemplate", auth, async (req, res) => {
    
    try{
    
        
      await Template.findByIdAndRemove({_id:req.body.tid})
      
      res.status(200).json({
          message:"Template deleted successfully"
        });
    }
    
    
   catch (e) {
    res.status(500).send("Internal server error");
  }
});




module.exports = router;
